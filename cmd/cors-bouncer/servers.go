package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"
	"sync/atomic"

	"golang.org/x/exp/maps"
)

type bounceEnd struct {
	redirect
	Username string `json:"username"`
	Password string `json:"password"`
}

type redirect struct {
	corsOptions
	path string

	Endpoint string `json:"endpoint"`
	File     string `json:"file"`
}

type serverConfig struct {
	Token    string                     `json:"token"`
	Redirect map[string]string          `json:"redirect"`
	Bounce   map[string]*bounceEnd      `json:"bounce"`
	Serve    map[string]json.RawMessage `json:"serve"`

	mux *http.ServeMux
}

func (sc *serverConfig) updateMux() {
	sc.mux = http.NewServeMux()

	for k, v := range sc.Bounce {
		v.path = k
		if v.File != "" {
			sc.mux.Handle("/"+k, v)
		} else {
			sc.mux.Handle("/"+k+"/", v)
		}
	}
	for k, v := range sc.Redirect {
		var r = &redirect{path: k, Endpoint: v}
		sc.mux.Handle("/"+k+"/", r)
	}
	for k, v := range sc.Serve {
		sc.mux.Handle("/"+k, &rawServe{Data: v})
	}
}

func (sc *serverConfig) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	fmt.Printf("serve mux\n")

	sc.mux.ServeHTTP(w, req)
}

type corsOptions struct{}

func (c *corsOptions) SetCORS(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", req.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Headers", "*")
}
func (c *corsOptions) SetOpt(w http.ResponseWriter, req *http.Request) {
	c.SetCORS(w, req)
	w.WriteHeader(http.StatusOK)
}

type bouncer struct {
	sync.RWMutex
	configs map[string]*serverConfig
	corsOptions
}

func (b *bouncer) Reset(s []serverConfig) {
	b.Lock()
	defer b.Unlock()
	if b.configs == nil {
		b.configs = map[string]*serverConfig{}
	}
	maps.Clear(b.configs)
	for _, c := range s {
		var otherC serverConfig = c
		otherC.updateMux()
		b.configs[c.Token] = &otherC
	}
}
func (b *bouncer) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	b.RLock()
	defer b.RUnlock()
	atomic.AddUint64(&requests, 1)
	if req.Method == http.MethodOptions {
		atomic.AddUint64(&servedRequests, 1)
		b.SetOpt(w, req)
		return
	}
	token := req.Header.Get("BouncerToken")
	if token == "" {
		token = req.URL.Query().Get("bouncer_token")
	}
	conf, ok := b.configs[token]
	if !ok {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	fmt.Println("got here")
	atomic.AddUint64(&servedRequests, 1)
	b.SetCORS(w, req)
	conf.ServeHTTP(w, req)
}

type rawServe struct {
	corsOptions
	Data []byte
}

func (r *rawServe) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	fmt.Printf("serve raw\n")
	if req.Method == http.MethodOptions {
		r.SetOpt(w, req)
		return
	}
	r.SetCORS(w, req)
	w.Header().Set("Content-Type", "application/json")
	w.Write(r.Data)
}

func (r *redirect) GetEndURL(req *http.Request) string {
	var endURL string
	if r.File != "" {
		endURL = r.File
	} else {
		path := strings.TrimPrefix(req.URL.Path, "/"+r.path)
		endURL = r.Endpoint + path
	}
	if req.URL.RawFragment != "" {
		endURL += "#" + req.URL.RawFragment
	}
	if req.URL.RawQuery != "" {
		v := req.URL.Query()
		if v.Has("bouncer_token") {
			v.Del("bouncer_token")
		}
		endURL += "?" + v.Encode()
	}
	return endURL
}
func (r *redirect) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	fmt.Printf("serve redirect\n")
	http.Redirect(w, req, r.GetEndURL(req), http.StatusTemporaryRedirect)
}
func (b *bounceEnd) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if b.Endpoint == "" {

		fmt.Printf("serve bounce at file %s\n", b.File)
	} else {

		fmt.Printf("serve bounce at %s\n", b.Endpoint)
	}
	// make request
	endReq, err := http.NewRequest(req.Method, b.GetEndURL(req), req.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, err)
		fmt.Printf("  invalid request\n")
		return
	}

	endReq.Header = req.Header.Clone()
	endReq.Header.Del("BouncerToken")
	if b.Username != "" {
		endReq.SetBasicAuth(b.Username, b.Password)
	}
	fmt.Printf("  Headers:\n")
	for k, v := range endReq.Header {
		for i, vv := range v {
			if i == 0 {
				fmt.Printf("    %s: %s\n", k, vv)
			} else {
				fmt.Printf("    %*s  %s\n", len(k), "", vv)
			}
		}
	}

	res, err := http.DefaultClient.Do(endReq)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, err)
		fmt.Printf("  error during request\n")
		return
	}
	fmt.Printf("  status: %s (%d)\n", res.Status, res.StatusCode)

	// copy result
	defer res.Body.Close()
	for k, vv := range res.Header {
		for _, v := range vv {
			w.Header().Add(k, v)
		}
	}
	w.WriteHeader(res.StatusCode)
	_, err = io.Copy(w, res.Body)
	// if err???
	_ = err
}
