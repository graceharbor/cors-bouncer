package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"encoding/pem"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/google/uuid"
)

var showHelp bool
var generateConfig bool
var configPath string

type config struct {
	Port         int            `json:"port"`
	Certificates []string       `json:"certificates"`
	Stats        string         `json:"stats"`
	Servers      []serverConfig `json:"servers"`
}

var conf config

func init() {
	flag.BoolVar(&showHelp, "help", false, "show help")
	flag.BoolVar(&showHelp, "h", false, "show help")
	flag.StringVar(&configPath, "config", "", "path to config file")
	flag.BoolVar(&generateConfig, "generate", false, "generate config and exit")
	flag.Parse()
}

// help shows help (invalid arg if argToShow, else exits cleanly)
func help(argToShow string) {
	if argToShow != "" {
		fmt.Fprintf(os.Stderr, "must specify -%s\n", argToShow)
	}
	fmt.Fprint(os.Stderr, "usage:\n    cors-bouncer [-h|-help] -token TOKEN -port PORT\nflags:\n")
	flag.PrintDefaults()
	if argToShow != "" {
		os.Exit(1)
	}
	os.Exit(0)
}

var requests uint64
var servedRequests uint64
var statsServed uint64
var start = time.Now()

func StatsServe(w http.ResponseWriter, req *http.Request) {
	atomic.AddUint64(&statsServed, 1)

	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, "This is CORS-bouncer")
	fmt.Fprintf(w, "  uptime:   %s\n", time.Since(start).Truncate(time.Second))
	fmt.Fprintf(w, "  port:     %d\n", conf.Port)
	fmt.Fprintf(w, "  stats:    %d\n", atomic.LoadUint64(&statsServed))
	fmt.Fprintf(w, "  requests: %d\n", atomic.LoadUint64(&requests))
	fmt.Fprintf(w, "  served:   %d\n", atomic.LoadUint64(&servedRequests))
	fmt.Fprintf(w, "  tokens:   %d\n", len(conf.Servers))
}

func main() {
	if showHelp {
		help("")
	}
	if configPath == "" {
		help("config")
	}
	if generateConfig {
		fmt.Println("generating config...")
		rand, _ := uuid.NewRandom()
		data, err := json.MarshalIndent(&config{
			Port:  1,
			Stats: "stats",
			Servers: []serverConfig{
				{Token: rand.String(),
					Redirect: map[string]string{
						"path/to/redirect": "http://example.com",
					},
					Bounce: map[string]*bounceEnd{
						"path/to/bounce": {redirect: redirect{Endpoint: "http://example.com"}, Username: "username", Password: "password"},
						"another":        {redirect: redirect{Endpoint: "http://example.com"}, Username: "username", Password: "password"},
					},
					Serve: map[string]json.RawMessage{
						"serve/json": json.RawMessage("\"these bytes directly\""),
					},
				},
			},
			Certificates: []string{"Enter your server certs here.", "Must be PEM encoded:", "  CERTIFICATE", "RSA PRIVATE KEY"},
		}, "", "  ")
		if err != nil {
			log.Fatal(err)
		}
		f, err := os.Create(configPath)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		_, err = f.Write(data)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("wrote config to %s\n", configPath)
		return
	}

	// Create new watcher.
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()
	err = watcher.Add(configPath)
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Open(configPath)
	if err != nil {
		log.Fatal(err)
	}
	func() {
		defer f.Close()
		err = json.NewDecoder(f).Decode(&conf)
		if err != nil {
			log.Fatal(err)
		}
	}()

	var tlsconfig *tls.Config
	if len(conf.Certificates) > 0 {
		var cert []byte
		var privKey []byte
		data := []byte(strings.Join(conf.Certificates, "\n"))
		for len(data) > 0 {
			var block *pem.Block
			block, data = pem.Decode(data)
			var pemBytes bytes.Buffer
			pem.Encode(&pemBytes, block)
			switch block.Type {
			case "CERTIFICATE":
				cert = pemBytes.Bytes()
			case "RSA PRIVATE KEY":
				privKey = pemBytes.Bytes()
			case "PRIVATE KEY":
				privKey = pemBytes.Bytes()
			}
		}
		var serverCert tls.Certificate
		serverCert, err = tls.X509KeyPair(cert, privKey)
		if err != nil {
			log.Fatal(err)
		}
		tlsconfig = &tls.Config{Certificates: []tls.Certificate{serverCert}}
	}

	fmt.Println("This is CORS-bouncer")
	fmt.Printf("  port:  %d\n", conf.Port)
	fmt.Printf("Watching config at:\n  %s\n", configPath)
	fmt.Println("Starting server")

	mux := http.NewServeMux()

	b := &bouncer{
		ConfigPath: configPath,
	}

	mux.HandleFunc("/"+conf.Stats, StatsServe)
	mux.HandleFunc("/", b.ServeHTTP)

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	srv := &http.Server{
		Addr:      fmt.Sprintf(":%d", conf.Port),
		TLSConfig: tlsconfig,
		Handler:   mux,
	}

	go func() {
		if tlsconfig != nil {
			err = srv.ListenAndServeTLS("", "")
		} else {
			err = srv.ListenAndServe()
		}
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
		err = nil
	}()
	go func() {
		var t = time.NewTimer(0)
		defer t.Stop()
		var s string = "loading"
		for {
			select {
			case <-t.C:
				f, err := os.Open(configPath)
				var conf2 config
				var n int64
				if err == nil {
					func() {
						defer f.Close()
						n, _ = f.Seek(0, io.SeekEnd)
						f.Seek(0, io.SeekStart)
						err = json.NewDecoder(f).Decode(&conf2)
					}()
				}
				if err != nil {
					fmt.Printf("  error reloading config: %s\n", err.Error())
				} else {
					fmt.Printf("  %s: found %d bytes\n", s, n)
					s = "reloading"
					conf = conf2
					b.Reset(conf.Servers)
				}
			case _, ok := <-watcher.Events:
				if !ok {
					return
				}
				t.Reset(time.Millisecond * 500)
			case _, ok := <-watcher.Errors:
				if !ok {
					return
				}
				// log.Println("error:", err)
			case <-done:
				return
			}
		}
	}()
	fmt.Println("server running...")
	<-done
	fmt.Println("shutting down...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("shutdown failed: %+v", err)
	}
	fmt.Println("done")
}
