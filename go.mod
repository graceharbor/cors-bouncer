module gitlab.com/graceharbor/cors-bouncer

go 1.20

require github.com/google/uuid v1.3.1

require (
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	golang.org/x/exp v0.0.0-20231006140011-7918f672742d
	golang.org/x/sys v0.13.0 // indirect
)
